//
//  StoryboardHandler.swift
//  SwiftInPractice
//
//  Created by Josh Campion on 25/07/2015.
//  Copyright © 2015 Josh Campion. All rights reserved.
//

import UIKit

public protocol SegueHandler {
    typealias SegueHandlerType: RawRepresentable
}

public extension SegueHandler where Self: UIViewController, SegueHandlerType.RawValue == String {
    
    func performSegue(identifier:SegueHandlerType, sender:AnyObject?) {
        performSegueWithIdentifier(identifier.rawValue, sender: sender)
    }
    
    /**
    
    - parameter: segue The segue being queried.
    - return: The SegueHandlerType enum for the given Storyboard segue.
    
    */
    func segueIdentifierForSegue(segue:UIStoryboardSegue) -> SegueHandlerType? {
        
        guard let identifier = segue.identifier else { return nil }
        
        guard let handler = SegueHandlerType(rawValue: identifier) else {
            fatalError("Unable to define SegueHandlerType \(SegueHandlerType.self) with identifier \(identifier)")
        }
        
        return handler
    }
}

public protocol StoryboardInstantiationHandler {
    typealias StoryboardInstantiationHandlerType:RawRepresentable
    
    /// Should be implemented by protocol either as self.storyboard or UIStoryboard(name:bundle:).
    func storyboardForIdentifier(identifier:StoryboardInstantiationHandlerType) -> UIStoryboard?
    
    /// Has default implementation. Should just be called.
    func instantiateViewController(identifier:StoryboardInstantiationHandlerType)
}

public extension StoryboardInstantiationHandler where StoryboardInstantiationHandlerType.RawValue == String {
    
    func instantiateViewController(identifier:StoryboardInstantiationHandlerType) {
        if let storyboard = storyboardForIdentifier(identifier) {
            storyboard.instantiateViewControllerWithIdentifier(identifier.rawValue)
        }
    }
    
}