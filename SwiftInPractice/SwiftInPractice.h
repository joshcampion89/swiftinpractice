//
//  SwiftInPractice.h
//  SwiftInPractice
//
//  Created by Josh Campion on 27/07/2015.
//  Copyright (c) 2015 Josh Campion. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SwiftInPractice.
FOUNDATION_EXPORT double SwiftInPracticeVersionNumber;

//! Project version string for SwiftInPractice.
FOUNDATION_EXPORT const unsigned char SwiftInPracticeVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftInPractice/PublicHeader.h>


