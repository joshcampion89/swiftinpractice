//
//  LocalizationHandler.swift
//  Pods
//
//  Created by Josh Campion on 05/08/2015.
//
//

import Foundation

public typealias HashableKey = protocol<RawRepresentable, Hashable>

/// Protocol to allow specification of localized Strings through enums. All details should be stored in `localStrings. Use the `dumpStringsFunction(_:_:)` (typically from the app delegate) to print out the static `NSLocalizedString()` code which can be used to generate the xliff file by Xcode.
public protocol LocalizationHandler {
    
    /// The enum to specify localization keys with.
    typealias KeyType:HashableKey
    
    /// Should contain all the localization strings to be used in an app if not using `allStrings` and `localizationForKey(_:)`
    static var localStrings:[KeyType: (value:String, comment:String)]? { get }
    
    static var allKeys:[KeyType]? { get }
    
    static func localizationForKey(key:KeyType) -> (value:String, comment:String)
}


public extension LocalizationHandler where KeyType.RawValue == String {
    
    static func LocalizedStringTableName(key:KeyType) -> String? {
        return nil
    }
    
    static func LocalizedStringBundle(key:KeyType) -> NSBundle {
        return NSBundle.mainBundle()
    }
    
    static func LocalizedStringValue(key:KeyType) -> String {
        
        if let strs = localStrings {
            return strs[key]?.value ?? ""
        } else {
            return localizationForKey(key).value
        }
    }
    
    static func LocalizedStringComment(key:KeyType) -> String {
        
        if let strs = localStrings {
            return strs[key]?.comment ?? ""
        } else {
            return localizationForKey(key).comment
        }
    }
    
    /**
     
     Prints to the console the code for a function equivalent to `NSLocalizedString(_:_:)` based on the `localStrings` property. For an implementation called `MyLocalization` with `KeyType = `MyLocalizationEnum`, this would call:
     
     func MyLocalizedString(key:MyLocalizationEnum) -> String {
     switch key {
     case .StringKey:
     return NSLocalizedStringKey(StringKey, value:"This is my string", comment:"This is my comment")
     default:
     return MyLocalization.localStrings[key]?.value ?? "Not Yet Set"
     }
     }
     
     - parameter named: The name of the function to be printed.
     - parameter type: The type of the enum that will be the `key` parameter of the printed function.
     - parameter class: The type of the object that contains the `localStrings` property.
     */
    public static func dumpStringsFunction(named:String, typed:String, classed:String, filePath:String? = nil) {
        var allStrings = "import Foundation\n\n/// Machine generated function enumerating \(typed) keys using a `switch` to ensure each key has an `NSLocalizedString(...)` entry.\npublic func \(named)(key:\(typed)) -> String {\n"
        allStrings += "switch key {\n"
        
        let localStrings:[KeyType:(value:String, comment:String)]
        
        if let allKeys = self.allKeys {
            
            let localStringsTuples = allKeys.map({ ($0, (value:self.LocalizedStringValue($0), comment: self.LocalizedStringComment($0))) })
            
            var strs = [KeyType: (value:String, comment:String)]()
            
            for t in localStringsTuples {
                strs[t.0] = t.1
            }
            
            localStrings = strs
            
        } else if let strs = self.localStrings {
            localStrings = strs
        } else {
            localStrings = [KeyType: (value:String, comment:String)]()
        }
        
        for (key, details) in localStrings {
            allStrings += "case .\(key):\n"
            allStrings += "return NSLocalizedString(\"\(key)\", "
            
            let escapedValue = details.value.stringByReplacingOccurrencesOfString("\n", withString: "\\n")
            
            allStrings += "value:\"\(escapedValue)\", "
            allStrings += "comment:\"\(details.comment)\")\n"
        }
        
        
        allStrings += "default:\nreturn \(classed).localStrings?[key]?.value ?? \"Not Yet Set\"\n"
        
        allStrings += "}\n}"
        
        if let path = filePath {
            do {
                try allStrings.writeToFile(path, atomically: true, encoding: NSUTF8StringEncoding)
            } catch let error {
                print("Failed to write localization to file: \(error)")
            }
        } else {
            print(allStrings)
        }
    }
}



