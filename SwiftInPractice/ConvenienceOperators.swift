//
//  ConvenienceOperators.swift
//  Pods
//
//  Created by Josh Campion on 06/08/2015.
//
//

import Foundation

infix operator !? { }

func !?<T>(wrapped: T?, @autoclosure nilDefault: ()->(value:T, text:String)) -> T {
    assert(wrapped != nil, nilDefault().text)
    return wrapped ?? nilDefault().value
}