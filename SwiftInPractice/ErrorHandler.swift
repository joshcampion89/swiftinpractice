//
//  ErrorHandler.swift
//  Syncopatica
//
//  Created by Josh Campion on 25/07/2015.
//  Copyright © 2015 Josh Campion. All rights reserved.
//

import Foundation

public protocol ErrorHandler {
    
    typealias ErrorDomainHandlerType: RawRepresentable
    
    typealias ErrorIdentifierHandlerType: RawRepresentable
    
//    func userInfoForDomain<KeyType:Hashable>(domain:ErrorDomainHandlerType, identifier:ErrorIdentifierHandlerType) -> [KeyType:AnyObject]?
    static func userInfoForDomain(domain:ErrorDomainHandlerType, identifier:ErrorIdentifierHandlerType, userInfo:[String:AnyObject]?) -> [String:AnyObject]?
}

public extension ErrorHandler where ErrorDomainHandlerType.RawValue == String, ErrorIdentifierHandlerType.RawValue == Int {
    
    static func createError(domain:Self.ErrorDomainHandlerType, identifier:Self.ErrorIdentifierHandlerType, userInfo:[String:AnyObject]? = nil) -> NSError {
        let userInfo = userInfoForDomain(domain, identifier: identifier, userInfo:userInfo)
        return NSError(domain: domain.rawValue, code: identifier.rawValue, userInfo: userInfo)
    }
}